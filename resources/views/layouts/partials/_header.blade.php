  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
          <li class="nav-item">
              <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
          </li>
      </ul>

      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
          <li class="nav-item">
              <div class="user-panel d-flex mt-1">
                  <div class="image">
                      <img src="{{ asset('vendors/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2"
                          alt="User Image">
                  </div>
                  <div class="info">
                      <a href="#" class="d-block">{{ Illuminate\Support\Facades\Auth::user()->fullname }}</a>
                  </div>
              </div>
          </li>
          <li class="nav-item">
              <a class="nav-link" style="color:#007bff" data-slide="true" role="button"
                  href="{{ route('logout') }}">Log out</a>
          </li>
      </ul>
  </nav>
  <!-- /.navbar -->
