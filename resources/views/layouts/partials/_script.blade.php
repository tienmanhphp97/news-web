<!-- Jquery -->
<script src="{{ asset('vendors/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('vendors/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- Admin lte -->
<script src="{{ asset('vendors/dist/js/adminlte.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
<!-- Toastr service -->
<script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
<!-- Summernote -->
<script src="{{ asset('plugins/summernote/summernote-bs4.min.js') }}"></script>

{{-- Sort table --}}
<script src="https://cdn.jsdelivr.net/gh/livewire/sortable@v0.x.x/dist/livewire-sortable.js"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
</script>

<script src="{{ asset('js/helper.js') }}"></script>
<!-- Customer javascript function -->
<script>
    window.addEventListener('show-toast', event => {
        if (event.detail.type == "success") {
            toastr.success(event.detail.message);
        } else if (event.detail.type == "error") {
            toastr.error(event.detail.message);
        }
    });
</script>
