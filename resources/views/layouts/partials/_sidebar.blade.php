  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="index3.html" class="brand-link">
          <img src="{{ asset('assets/images/logo.jpg') }}" alt="AdminLTE Logo"
              class="brand-image img-circle elevation-3" style="opacity: .8">
          <span class="brand-text font-weight-light">TAN SON</span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
          <!-- Sidebar user panel (optional) -->
          {{-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
              <div class="image">
                  <img src="{{ asset('vendors/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2"
                      alt="User Image">
              </div>
              <div class="info">
                  <a href="#" class="d-block">{{Illuminate\Support\Facades\Auth::user()->fullname}}</a>
              </div>
          </div> --}}
          <nav class="mt-2">
              <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                  data-accordion="false">
                  {{-- <li class="nav-item menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Starter Pages
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="#" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Active Page</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Inactive Page</p>
                </a>
              </li>
            </ul>
          </li> --}}
                  <li class="nav-item">
                      <a href="{{ route('user') }}" class="nav-link {{ setActive('user') }}">
                          <i class="fas fa-user"></i>
                          <p>
                              Quản lý người dùng
                          </p>
                      </a>
                  </li>
                  {{-- <li class="nav-item">
                      <a href="{{ route('tag') }}" class="nav-link {{ setActive('tags') }}">
                          <i class="fas fa-tags"></i>
                          <p>
                              Quản lý thẻ
                          </p>
                      </a>
                  </li> --}}
                  <li class="nav-item">
                      <a href="{{ route('news') }}" class="nav-link {{ setActive('news') }}">
                        <i class="fas fa-newspaper"></i>
                          <p>
                              Quản lý tin tức
                          </p>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a href="{{route('category')}}" class="nav-link {{setActive('categories')}}">
                          <i class="fas fa-building"></i>
                          <p>
                              Quản lý danh mục
                          </p>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a href="{{route('feedback')}}" class="nav-link {{setActive('feedback')}}">
                          <i class="fas fa-comments"></i>
                          <p>
                              Quản lý phản hồi
                          </p>
                      </a>
                  </li>
              </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
  </aside>
