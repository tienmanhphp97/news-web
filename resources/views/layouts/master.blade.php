<!DOCTYPE html>
<html lang="en">
@include('layouts.partials._head')
@yield('css')
<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        @include('layouts.partials._header')
        @include('layouts.partials._sidebar')
        <div class="content-wrapper">
            <section class="content">
                @yield('content')
            </section>
        </div>
        <aside class="control-sidebar control-sidebar-dark">
            <div class="p-3">
                <h5>Title</h5>
                <p>Sidebar content</p>
            </div>
        </aside>
        @include('layouts.partials._footer')
        <!-- /.main-main-container -->
        @include('layouts.partials._script')
        @yield('js')
    </div>
</body>
</html>
