<div class="page-content fade-in-up">
    <div class="p-2 pb-3 d-flex align-items-center justify-content-between">
        <div class="">
            <h4 class="m-0 ml-2">
                Danh sách thẻ
            </h4>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row mb-3">
            <div class="col-7 d-flex">
                    <input wire:model.debounce.1000ms="searchName" id="searchName" name="searchName"
                           placeholder="Nhập từ khóa tìm kiếm"type="text" class="form-control col-6">
                <div class="col-2">
                    <div style="margin-left:5px;float: left;text-align: center;">
                        <button type="button" class="btn btn-secondary" wire:click="resetSearch()"
                                title="Làm mới"><i class="fa fa-undo"></i> </button>
                    </div>
                </div>
            </div>
            <div class="col-5 pr-0" style="text-align:right">
                <button data-toggle="modal" data-target="#modelCreateEdit" type="button" class="btn btn-primary mr-3" wire:click="create()">Tạo mới</button></a></li>
            </div>
        </div>
        <div class="card">
            <div class="row card-body">
                <div class="col-sm-12">
                    <table class="table table-bordered table-striped dataTable dtr-inline"
                           id="category-table" cellspacing="0" width="100%" role="grid"
                           aria-describedby="category-table_info" style="width: 100%;">
                        <thead>
                        <tr role="row">
                            <th class="text-center" scope="col">#</th>
                            <th class="text-center" scope="col">Tên thẻ</th>
                            <th class="text-center" scope="col">Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $row)
                            <tr data-parent="" data-index="1" role="row" class="odd">
                                <th class="text-center" scope="row">{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</th>
                                <td class="text-center">
                                    <a data-toggle="modal" data-target="#modelCreateEdit" title="{{ $row->name }}" wire:click="show({{$row->id}})">{{ $row->name }}</a>
                                </td>
                                <td class="text-center">
                                    <a type="button" data-toggle="modal" data-target="#modelCreateEdit" title="Sửa" style="font-size: 10px" wire:click="edit({{$row->id}})"
                                        class="btn btn-primary btn-xs text-white"><i
                                            class="fas fa-pencil-alt"></i></a>
                                    @include('livewire.common.buttons._delete')

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @if (!isset($data) || !count($data))
                        <div class="pb-2 pt-3 text-center">Không tìm thấy dữ liệu</div>
                    @endif
                </div>
                {{ $data->links() }}
            </div>
            <div wire:ignore.self class="modal fade" id="modelCreateEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content p-3">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">{{$this->mode=='create'?"Thêm mới":($this->mode=='show'?"Chi tiết":"Chỉnh sửa")}} thẻ</h5>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label> Tên thẻ</label>
                                <input type="text"  class="form-control" placeholder="Tên tag" wire:model.defer="name" {{$mode=="show"?"disabled":""}}>
                                @error("name")
                                @include("admin.layouts.text._error")
                                @enderror
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" >Đóng</button>
                            @if($this->mode!='show')
                            <button type="button"  class="btn btn-primary"  wire:click='saveData'>Lưu</button>
                                @endif
                        </div>
                    </div>
                </div>
            </div>
            @include('livewire.common.modals._modalDelete')
        </div>
    </div>
</div>
