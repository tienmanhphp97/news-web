<div class="page-content fade-in-up">
    <div class="p-2 pb-3 d-flex align-items-center justify-content-between">
        <div class="">
            <h4 class="m-0 ml-2">
                Danh sách phản hồi
            </h4>
        </div>
    </div>
    <div class="container-fluid">
            <div class="row mb-3">
                <div class="col-7 d-flex">
                    <input wire:model.debounce.1000ms="searchName" id="searchName" name="searchName"
                               placeholder="Nhập từ khóa tìm kiếm"type="text" class="form-control col-6">
                    <div class="col-6">
                        <select class="form-control" wire:model="searchStatus" >
                            <option value="">---Trạng thái---</option>
                            <option value="1">Đã trả lời</option>
                            <option value="0">Chưa trả lời</option>
                        </select>
                    </div>

                    <div class="col-2">
                            <button type="button" class="btn btn-secondary" wire:click="resetSearch()"
                                    title="Làm mới"><i class="fa fa-undo"></i> </button>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="row card-body">
                    <div class="col-sm-12">
                        <table class="table table-bordered table-striped dataTable dtr-inline"
                               id="category-table" cellspacing="0" width="100%" role="grid"
                               aria-describedby="category-table_info" style="width: 100%;">
                            <thead>
                            <tr role="row">
                                <th class="text-center" scope="col">#</th>
                                <th class="text-center" scope="col">Phản hồi</th>
                                <th class="text-center" scope="col">Số điện thoại</th>
                                <th class="text-center" scope="col">Email</th>
                                <th class="text-center" scope="col">Trạng thái</th>
                                <th class="text-center" scope="col">Thao tác</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($data as $row)
                                <tr data-parent="" data-index="1" role="row" class="odd">
                                    <th class="text-center" scope="row">{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</th>
                                    <td class="text-center">{{ $row->content }}
                                    </td>
                                    <td class="text-center">{{ $row->phone }}</td>
                                    <td class="text-center">{{ $row->email }}</td>
                                    <td class="text-center">{{ $row->status?'Đã trả lời':'Chưa trả lời' }}</td>
                                    <td class="text-center">
                                        <a type="button" data-toggle="modal" data-target="#modelCreateEdit" title="Sửa" style="font-size: 10px" wire:click="feedback({{$row->id}})"
                                           class="btn btn-primary btn-xs text-white"><i
                                                class="fas fa-paper-plane"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @if (!isset($data) || !count($data))
                            <div class="pb-2 pt-3 text-center">Không tìm thấy dữ liệu</div>
                        @endif
                    </div>
                    {{ $data->links() }}
                </div>
                <div wire:ignore.self class="modal fade" id="modelCreateEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content p-3">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Phản hồi</h5>
                            </div>
                            <div class="modal-body">
                                @if($feedback)
                                <div>
                                    <div class="comment col-7 mt-4 text-justify float-left">
                                        <img src="{{asset('images/avatar_default.jpg')}}" alt="" class="rounded-circle border-1" width="40" height="40">
                                        <h6>{{$feedback->email}}</h6>
                                        <span class="timeline-footer"> {{time_elapsed_string($feedback->created_at)}}</span>
                                        <br>
                                        <p>{{$feedback->content}}</p>
                                    </div>
                                    @foreach($feedback->children  as $reply)
                                    <div class="text-justify col-7 darker mt-4 float-right">
                                        @if($mode!='edit'||$editId!=$reply->id)
                                            <h6>Admin</h6>
                                            <span class="timeline-footer"> {{time_elapsed_string($reply->created_at)}}</span>
                                            <a class="edit-feedback" wire:click="edit({{$reply->id}},'{{$reply->content}}')"><i class="fas fa-pen"></i></a>
                                            <br>
                                            <p>{{$reply->content}}</p>
                                        @else
                                            <textarea class="border-0" style="width: 100%" rows="3" wire:model.defer="content_edit"></textarea>
                                        @endif
                                    </div>
                                    @if($mode=='edit'&&$editId==$reply->id)
                                    <div class="text-right col-7 mt-2 float-right p-0">
                                        <div class="text-left">
                                            @error("content_edit")
                                            @include("admin.layouts.text._error")
                                            @enderror
                                        </div>
                                        <button type="button"  class="btn btn-primary"  wire:click='saveData'>Lưu</button>
                                        <button type="button"  class="btn btn-light border"  wire:click='cancel()'>Hủy</button>
                                    </div>
                                    @endif
                                    @endforeach
                                    <div class="text-justify col-7 darker mt-4 float-right" style="padding: 0 5px" >
                                            <textarea class="border-0" style="width: 100%" rows="3" wire:model.defer="content"></textarea>
                                    </div>
                                    <div class="text-right col-7 mt-2 float-right p-0">
                                        <div class="text-left">
                                        @error("content")
                                        @include("admin.layouts.text._error")
                                        @enderror
                                        </div>
                                    <button type="button"  class="btn btn-primary"  wire:click='saveData'>Gửi</button>
                                </div>
                                    @endif
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal" >Đóng</button>
{{--                                @if($this->mode!='show')--}}
{{--                                    <button type="button"  class="btn btn-primary"  wire:click='saveData'>Lưu</button>--}}
{{--                                @endif--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

