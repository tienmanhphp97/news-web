<div class="page-content fade-in-up">
    <div class="p-2 pb-3 d-flex align-items-center justify-content-between">
        <div class="">
            <h4 class="m-0 ml-2">
                Danh sách danh mục
            </h4>
        </div>
    </div>

        <div class="container-fluid">
            <div class="row mb-3">
                <div class="col-7 d-flex">
                        <input wire:model.debounce.1000ms="searchName" id="searchName" name="searchName"
                               placeholder="Nhập từ khóa tìm kiếm"type="text" class="form-control col-6">
                    <div class="col-2">
                        <div style="margin-left:5px;float: left;text-align: center;">
                            <button type="button" class="btn btn-secondary" wire:click="resetSearch()"
                                    title="Làm mới"><i class="fa fa-undo"></i> </button>
                        </div>
                    </div>
                </div>
                <div class="col-5 pr-0" style="text-align:right">
                    <button data-toggle="modal" data-target="#modelCreateEdit" type="button" class="btn btn-primary mr-3" wire:click="create()">Tạo mới</button></a></li>
                </div>
            </div>
            <div class="card">
                <div class="row card-body">
                    <div class="col-sm-12">
                        <table class="table table-bordered table-striped dataTable dtr-inline"
                               id="category-table" cellspacing="0" width="100%" role="grid"
                               aria-describedby="category-table_info" style="width: 100%;">
                            <thead class="">
                            <tr role="row">
                                <th></th>
                                <th class="text-center" scope="col">#</th>
                                <th class="text-center" scope="col">Tên</th>
                                <th class="text-center" scope="col">Danh mục cha</th>
                                <th class="text-center" scope="col">Tag</th>
                                <th class="text-center" scope="col">Đường dẫn</th>
                                <th class="text-center" scope="col">Mô tả</th>
                                <th class="text-center" scope="col">Thao tác</th>
                            </tr>
                            </thead>
                            <tbody wire:sortable="updateOrder">
                            @foreach ($data as $row)
                                <tr wire:sortable.item="{{$row->order}}" data-parent="" data-index="1" role="row" class="odd">
                                    <td class="text-center" style="cursor: move; "  wire:sortable.handle><i class="fas fa-arrows-alt"></i></td>
                                    <th class="text-center" scope="row">{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</th>
                                    <td class="text-center">
                                        <a data-toggle="modal" data-target="#modelCreateEdit" title="{{ $row->name }}" wire:click="show({{$row->id}})">{{ $row->name }}</a>
                                    </td >
                                    <td class="text-center">{{ $row->parent?$row->parent->name:'' }}</td>
                                    <td class="text-center">{{ $row->tagName }}</td>
                                    <td>{{ $row->link }}</td>
                                    <td>{{$row->description}}</td>
                                    <td class="text-center">
                                        <a type="button" data-toggle="modal" data-target="#modelCreateEdit" title="Sửa" style="font-size: 10px" wire:click="edit({{$row->id}})"
                                           class="btn btn-primary btn-xs text-white"><i
                                                class="fas fa-pencil-alt"></i></a>
                                        @include('livewire.common.buttons._delete')

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @if (!isset($data) || !count($data))
                            <div class="pb-2 pt-3 text-center">Không tìm thấy dữ liệu</div>
                        @endif
                    </div>
                    {{ $data->links() }}
                </div>
                <div wire:ignore.self class="modal fade" id="modelCreateEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content p-3">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">{{$this->mode=='create'?"Thêm mới":($this->mode=='show'?"Chi tiết":"Chỉnh sửa")}} danh mục</h5>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label> Tên danh mục</label>
                                    <input type="text"  class="form-control" placeholder="Tên danh mục" wire:model.defer="name" {{$mode=="show"?"disabled":""}}>
                                    @error("name")
                                    @include("admin.layouts.text._error")
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label> Danh mục cha</label>
                                    <select class="form-control" wire:model.defer="parent_id" {{$mode=="show"?"disabled":""}}>
                                        <option value="">---Danh mục cha---</option>
                                        @foreach($categories as $value)
                                            <option value="{{$value->id}}">{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                    @error("parent_id")
                                    @include("admin.layouts.text._error")
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label> Thẻ</label>
                                    <select class="form-control" wire:model.defer="tag_id" {{$mode=="show"?"disabled":""}}>
                                        <option value="" hidden>---Thẻ---</option>
                                        @foreach($tag as $value)
                                        <option value="{{$value->id}}">{{$value->name}}</option>
                                        @endforeach
                                    </select>
                                    @error("tag_id")
                                    @include("admin.layouts.text._error")
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label> Đường dẫn</label>
                                    <input type="text"  class="form-control" placeholder="Link" wire:model.defer="link" {{$mode=="show"?"disabled":""}}>
                                    @error("link")
                                    @include("admin.layouts.text._error")
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label>Is Memu</label><br>
                                    {{-- <input type="text"  class="form-control" placeholder="Link" wire:model.defer="link" {{$mode=="show"?"disabled":""}}> --}}
                                    <input type="checkbox" wire:model.defer="is_menu" {{$mode=="show"?"disabled":""}}>
                                </div>
                                <div class="form-group">
                                    <label>Mô tả</label><br>
                                    {{-- <input type="checkbox" wire:model.defer="description" {{$mode=="show"?"disabled":""}}> --}}
                                    <textarea class="form-control" wire:model.defer="description" style="height: 160px"  cols="101"  {{$mode=="show"?"disabled":""}}></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal" >Đóng</button>
                                @if($this->mode!='show')
                                    <button type="button"  class="btn btn-primary"  wire:click='saveData'>Lưu</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @include('livewire.common.modals._modalDelete')
            </div>
    </div>
</div>

