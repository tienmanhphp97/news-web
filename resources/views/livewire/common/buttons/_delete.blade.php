{{--<button type="button" data-toggle="modal" data-target="#deleteModal" title="{{__('common.button.delete')}}" wire:click="deleteId({{$row->id}})" class="btn-sm border-0 bg-transparent"><img src="/images/trash.svg" alt="trash"></button>--}}
<a data-toggle="modal" data-target="#deleteModal" title="Xóa" style="font-size: 10px" wire:click="deleteId({{$row->id}})"
    class="btn btn-danger delete-category btn-xs text-white"><i
        class="fa fa-trash font-14"></i></a>
