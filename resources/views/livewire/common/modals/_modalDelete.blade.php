<div wire:ignore.self class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content p-3">
            <div class="modal-header box-user p-0">
                <h4 class="modal-title">Xác nhận xóa</h4>
                <button class="btn float-right" style="position: absolute; top: 0px; right: 0px;" data-dismiss="modal"><em class="fa fa-times"></em></button>
            </div>
            <div class="modal-body">

                Bạn có muốn xóa bản ghi này không?
            </div>
            <div class="modal-footer group-btn2 text-center">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                <button type="button" style="margin-left: 10px" wire:click.prevent="delete()" class="btn btn-primary">Xóa</button>
            </div>
        </div>
    </div>
</div>
