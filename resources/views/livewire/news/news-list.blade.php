<div class="page-content fade-in-up">
    <div class="p-2 pb-3 d-flex align-items-center justify-content-between">
        <div class="">
            <h4 class="m-0 ml-2">
                Danh sách tin tức
            </h4>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row mb-3">
            <div class="col-7 d-flex">
                <input wire:model.debounce.1000ms="searchName" id="searchName" name="searchName"
                    placeholder="Nhập từ khóa tìm kiếm"type="text" class="form-control col-6">
                <div class="col-6 d-flex">
                    <select name="category_id" id="category_id" wire:model.debounce.1000ms="searchCate"
                        class="form-control">
                        <option value="" selected>Chọn danh mục</option>
                        @foreach ($cates as $value)
                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                        @endforeach
                    </select>
                    <div style="margin-left:20px;float: left;text-align: center;">
                        <button type="button" class="btn btn-secondary" wire:click="resetSearch()" title="Làm mới"><i
                                class="fa fa-undo"></i> </button>
                    </div>
                </div>
            </div>
            <div class="col-5 pr-0" style="text-align:right">
                <button data-toggle="modal" id="create" data-target="#modelCreateEdit" type="button"
                    class="btn btn-primary mr-3" wire:click="create()">Tạo mới</button></a></li>
            </div>
        </div>
        <div class="card">
            <div class="row card-body modal-open">
                <div class="col-sm-12">
                    <table class="table table-bordered table-striped dataTable dtr-inline" id="category-table"
                        cellspacing="0" width="100%" role="grid" aria-describedby="category-table_info"
                        style="width: 100%;">
                        <thead>
                            <tr role="row">
                                <th class="text-center" scope="col">#</th>
                                <th class="text-center" scope="col">Tiêu đề</th>
                                <th class="text-center" scope="col">Danh mục</th>
                                <th class="text-center" scope="col">Miêu tả</th>
                                <th class="text-center" scope="col">Tệp đính kèm</th>
                                <th class="text-center" scope="col">Thao tác</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $row)
                                <tr data-parent="" data-index="1" role="row" class="odd">
                                    <th class="text-center" scope="row">
                                        {{ ($data->currentPage() - 1) * $data->perPage() + $loop->iteration }}</th>
                                    <td class="text-center" wire:click="show({{ $row->id }})" data-toggle="modal"
                                        data-target="#modelCreateEdit">{{ $row->name ?? '' }}
                                    </td>
                                    <td class="text-center" wire:click="show({{ $row->id }})" data-toggle="modal"
                                        data-target="#modelCreateEdit">
                                        {{ $row->category->name ?? '' }}
                                    </td>
                                    {{-- <td class="text-center" wire:click="show({{ $row->id }})" data-toggle="modal"
                                        data-target="#modelCreateEdit">
                                        {{ preg_replace('/<(?:[^\>]+)>/', ' ', $row->content ?: '') }}
                                    </td> --}}
                                    <td class="text-center" wire:click="show({{ $row->id }})" data-toggle="modal"
                                        data-target="#modelCreateEdit">{{ $row->description ?? '' }}
                                    </td>
                                    <td class="text-center">
                                        @if ($row->file_path)
                                            <img style="height: 90px;width:90px"
                                                src="{{$row->file_path }}" alt="">
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <a data-toggle="modal" id="detail" data-target="#modelCreateEdit"
                                            data-toggle="tooltip" wire:click="edit({{ $row->id }})" title="Sửa"
                                            style="font-size: 10px" class="btn btn-primary btn-xs text-white"><i
                                                class="fas fa-pencil-alt"></i></a>
                                        @include('livewire.common.buttons._delete')
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @if (!isset($data) || !count($data))
                        <div class="pb-2 pt-3 text-center">Không tìm thấy dữ liệu</div>
                    @endif
                </div>
                {{ $data->links() }}
            </div>
            <div wire:ignore.self class="modal fade" style="overflow-y: auto" id="modelCreateEdit" tabindex="-1"
                role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content p-3">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">
                                {{ $this->mode == 'create' ? 'Thêm mới' : ($this->mode == 'show' ? 'Chi tiết' : 'Chỉnh sửa') }}
                                tin tức</h5>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label> Tiêu đề <span class="text-danger">*</span></label>
                                <input {{ $mode == 'show' ? 'disabled' : '' }} type="text" class="form-control"
                                    placeholder="Tiêu đề" wire:model.defer="name">
                                @error('name')
                                    @include('admin.layouts.text._error')
                                @enderror
                            </div>
                            <div class="form-group" wire:ignore>
                                <label> Nội dung </label>
                                {{-- <input {{ $mode == 'show' ? 'disabled' : '' }} type="text"
                                        class="form-control" id="summernote" placeholder="Nội dung"
                                        wire:model.defer="content"> --}}
                                <textarea {{ $mode == 'show' ? 'disabled' : '' }} placeholder="Mô tả"
                                    class="form-control custom-input-control textarea w-100" id="content" name="content" rows="5">
                                     {{ $content }}
                                    </textarea>
                                @error('content')
                                    @include('admin.layouts.text._error')
                                @enderror
                            </div>
                            <div class="form-group">
                                <label> Danh mục <span class="text-danger">*</span></label>
                                <select {{ $mode == 'show' ? 'disabled' : '' }} name="category_id" id="category_id"
                                    wire:model.defer="category_id" class="form-control">
                                    <option value="" selected>Chọn danh mục</option>
                                    @foreach ($cates as $value)
                                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                                    @endforeach
                                </select>
                                @error('category_id')
                                    @include('admin.layouts.text._error')
                                @enderror
                            </div>
                            <div class="form-group">
                                <label> Mô tả</label>
                                <input placeholder="Mô tả" {{ $mode == 'show' ? 'disabled' : '' }} type="text"
                                    class="form-control" wire:model.defer="description">
                                @error('description')
                                    @include('admin.layouts.text._error')
                                @enderror
                            </div>
                            <div class="form-group">
                                <label> Meta title</label>
                                <input placeholder="Meta title" {{ $mode == 'show' ? 'disabled' : '' }}
                                    type="text" class="form-control" placeholder="Email"
                                    wire:model.defer="meta_title">
                            </div>
                            <div class="form-group">
                                <label> Meta description {{ $this->mode == 'edit' ? 'mới' : '' }}</label>
                                <textarea {{ $mode == 'show' ? 'disabled' : '' }} placeholder="Meta description"
                                    class=" form-control custom-input-control textarea w-100" id="meta_description" name="meta_description"
                                    rows="5" wire:model.defer='meta_description'>
                                    {{ $meta_description }}
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label> Meta keyword </label>
                                <input placeholder="Meta keyword" {{ $mode == 'show' ? 'disabled' : '' }}
                                    type="text" class="form-control" placeholder="Mật khẩu"
                                    wire:model.defer="meta_keyword">
                            </div>
                            <div class="form-group">
                                <label> Tệp đính kèm </label>
                                <form wire:submit.prevent="deleteImage">
                                    <input placeholder="Meta keyword" {{ $mode == 'show' ? 'disabled' : '' }}
                                        type="file" class="form-control" placeholder="Mật khẩu"
                                        wire:model.defer="url">
                                    @if ($url)
                                        <div>
                                            <img style="width:90px;height:90px" src="{{ $url->temporaryUrl() }}">
                                            <button wire:click="deleteImage"
                                                style="border:unset;background: unset;"><i
                                                    class="fas fa-window-close"></i></button>
                                        </div>
                                    @endif
                                    @if ($oldUrl)
                                        <div>
                                            <img style="width:90px;height:90px" src="{{ $oldUrl }}">
                                            <button wire:click="deleteOldImage"
                                                style="border:unset;background: unset;"><i
                                                    class="fas fa-window-close"></i></button>
                                        </div>
                                    @endif
                                </form>
                                @error('url')
                                    @include('admin.layouts.text._error')
                                @enderror
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                            @if ($this->mode != 'show')
                                <button id="btn-save" type="button" class="btn btn-primary"
                                    wire:click='saveData'>Lưu</button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('livewire.common.modals._modalDelete')
    </div>
</div>

<script>
    var editorSummernote = $('#content');
    window.livewire.on('setContent', () => {
        editorSummernote.summernote('code', '');
    });
    window.livewire.on('setContentEdit', (content) => {
        editorSummernote.summernote('code', content);
    });
    editorSummernote.summernote({
        fontSizes: ['8', '9', '10', '11', '12', '14', '18', '24', '36', '48', '64', '82', '150'],
        callbacks: {
            onChange: function(content, $editable) {
                @this.set('content', content);
            },
            onImageUpload: function(files, editor) {
                var formData = new FormData();
                var file = files[0]
                formData.append('file', file)
                $.ajax({
                    type: 'POST',
                    data: formData,
                    url: '/api/upload',
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        var node = document.createElement('img');
                        $(node).attr('src', data)
                        editorSummernote.summernote('insertNode', node);
                    }
                });
            }
        },
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'superscript', 'subscript',
                'strikethrough', 'clear'
            ]],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video', 'hr']],
            ['view', ['codeview']]
        ],
        tabsize: 2,
        height: 200,
    });
</script>
