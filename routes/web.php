<?php

use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Feedback\FeedbackController;
use App\Http\Controllers\News\NewsController;
use App\Http\Controllers\Category\CategoryController;
use App\Http\Controllers\Tag\TagController;
use App\Http\Controllers\User\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['auth']], function () {
    Route::get('/', function () {
        return view('users/user-list');
    });
    Route::get('dashboard',[DashboardController::class,'dashboard'])->name('dashboard');
    Route::get('/users',[UserController::class,'index'])->name('user');
    Route::get('/tags',[TagController::class,'index'])->name('tag');
    Route::get('/news',[NewsController::class,'index'])->name('news');
    Route::get('/categories',[CategoryController::class,'index'])->name('category');
    Route::get('/feedback',[FeedbackController::class,'index'])->name('feedback');
});
Route::get('/login',[AuthController::class,'getLogin'])->name('login');
Route::post('/login',[AuthController::class,'postLogin'])->name('postLogin');
Route::get('/logout',[DashboardController::class,'logout'])->name('logout');
