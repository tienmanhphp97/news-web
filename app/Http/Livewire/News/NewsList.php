<?php

namespace App\Http\Livewire\News;

use App\Http\Livewire\Base\BaseLive;
use App\Models\Category;
use App\Models\News;
use App\Models\Tag;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class NewsList extends BaseLive
{
    use WithFileUploads;

    public $name;
    public $content;
    public $category_id;
    public $description;
    public $meta_title;
    public $meta_description;
    public $meta_keyword;
    public $mode = 'create';
    public $newsId;
    public $newsList;
    public $searchName;
    public $searchCate;
    public $url;
    public $oldUrl;
    public $dataNew;
    protected $listeners = [
        'set-content' => 'setContent',
    ];

    protected $rules = [
        'name' => 'required',
        'category_id' => 'required',
        'url' => 'nullable|mimes:jpg,png,jpeg|max:2048',
    ];
    protected $messages = [
        'name.required' => 'Tiêu đề bắt buộc',
        'category_id.required' => 'Danh mục bắt buộc',
        'url.max' => 'Dung lượng file đính kèm không được quá 2MB'
    ];

    public function setContent($content)
    {
        $this->content = $content;
        $this->saveData();
    }
    public function render()
    {
        $cates = Category::get();
        $query = News::query()->with('category');
        if ($this->searchName) {
            $query->where("name", "like", "%" . $this->searchName . "%");
            $query->orWhere("content", "like", "%" . $this->searchName . "%");
            $query->orWhere("description", "like", "%" . $this->searchName . "%");
        }
        if ($this->searchCate) {
            $query->where('category_id', $this->searchCate);
        }
        $data = $query->orderBy('created_at', 'desc')->paginate($this->perPage);
        return view('livewire.news.news-list', compact('data', 'cates'));
    }

    public function create()
    {
        $this->mode = 'create';
        $this->resetInput();
    }

    public function saveData()
    {
        $this->validate();
        if ($this->mode == 'create') {
            $this->newsList =  new News();
        } else if ($this->mode == 'edit') {
            $this->newsList =  News::findOrFail($this->newsId);
        }
        if (!$this->oldUrl) {
            $this->newsList->file_path = null;
        }
        $this->newsList->name = $this->name;
        $this->newsList->content = $this->content;
        $this->newsList->category_id = $this->category_id;
        $this->newsList->description = $this->description;
        $this->newsList->meta_title = $this->meta_title;
        $this->newsList->meta_description = $this->meta_description;
        $this->newsList->meta_keyword = $this->meta_keyword;
        if ($this->url) {
            $this->newsList->file_path = Storage::url($this->url->store('images', 'public'));
        }
        $this->newsList->save();
        $this->emit('closeModalCreateEdit');
        $this->emit('saveSum', $this->content);
        $this->resetInput();
        if ($this->mode == 'create') {
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => 'Thêm thành công']);
        } else if ($this->mode == 'edit') {
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => 'Sửa thành công']);
        }
    }
    public function edit($id)
    {
        $this->emit('setValueSummernote');
        $this->mode = 'edit';
        $this->newsId = $id;
        $this->resetValidation();
        $this->dataNew =  News::findOrFail($this->newsId);
        $this->name = $this->dataNew->name;
        $this->content = $this->dataNew->content;
        $this->category_id = $this->dataNew->category_id;
        $this->description = $this->dataNew->description;
        $this->meta_title = $this->dataNew->meta_title;
        $this->meta_description = $this->dataNew->meta_description;
        $this->meta_keyword = $this->dataNew->meta_keyword;
        $this->oldUrl = $this->dataNew->file_path;
        $this->emit('setContentEdit', $this->content);
    }
    public function resetInput()
    {
        $this->name = '';
        $this->content = '';
        $this->category_id = null;
        $this->description = '';
        $this->meta_title = '';
        $this->meta_description = '';
        $this->meta_keyword = '';
        $this->url = '';
        $this->oldUrl = '';
        $this->emit('setContent');
    }
    public function show($id)
    {
        $this->mode = 'show';
        $this->newsList =  News::findOrFail($id);
        if ($this->newsList) {
            $this->name = $this->newsList->name;
            $this->content = $this->newsList->content;
            $this->category_id = $this->newsList->category_id;
            $this->description = $this->newsList->description;
            $this->meta_title = $this->newsList->meta_title;
            $this->meta_description = $this->newsList->meta_description;
            $this->meta_keyword = $this->newsList->meta_keyword;
            $this->oldUrl = $this->newsList->file_path;
        }
    }
    public function resetSearch()
    {
        $this->searchName = '';
        $this->searchCate = '';
    }
    public function delete()
    {
        News::findOrFail($this->deleteId)->delete();
        $this->emit('closeModalDelete');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => 'Xóa thành công']);
    }
    public function updatedUrl()
    {
        $this->oldUrl = '';
    }
    public function deleteImage()
    {
        $this->url = null;
    }
    public function deleteOldImage()
    {
        $this->oldUrl = null;
        $this->url = null;
    }
}
