<?php

namespace App\Http\Livewire\Categories;

use App\Http\Livewire\Base\BaseLive;
use App\Models\Category;
use App\Models\Tag;
use Livewire\Component;
use DB;

class CategoryList extends BaseLive
{
    public $mode = 'create';
    public $name;
    public $tag_id;
    public $parent_id;
    public $link;
    public $is_menu;
    public $searchName;
    public $categoryId;
    public $description;
    public $order;
    public $dataSortable = [];
    protected $rules = [
        'name' => 'required',
    ];

    protected $messages = [
        'name.required' => 'Tên category bắt buộc',
    ];
    public function render()
    {
        $categories = Category::query()->whereNot('name', 'Giới thiệu công ty')->whereNot('name', 'Profile')->get();
        $tag = Tag::get();
        $query = Category::query();
        if ($this->searchName) {
            $query->where("category.name", "like", "%" . $this->searchName . "%");
        }
        $query->leftJoin('tags', 'tags.id', 'category.tag_id')->select('category.*', DB::raw('tags.name as tagName'));
        $query->with('parent');
        //        dd($query->get());
        $data = $query->orderBy('order', 'asc')->paginate($this->perPage);
        if ($data->total() > 0) {
            $arrSortable = $data->toArray();
            $this->dataSortable = $arrSortable['data'];
        }
        return view('livewire.categories.category-list', compact('data', 'categories', 'tag'));
    }
    public function create()
    {
        // $this->resetInput();
        $this->resetValidation();
        $this->mode = 'create';
    }
    public function saveData()
    {
        $orderCate = Category::pluck('order')->toArray();
        $lastOrderCate =array_pop($orderCate);
        // dd($lastOrderCate);
        $this->validate();
        $this->standardData();
        if ($this->mode == 'create') {
            $this->category =  new Category();
        } else if ($this->mode == 'edit') {
            $this->category =  Category::find($this->categoryId);
        }
        $this->category->name = $this->name;
        $this->category->parent_id = $this->parent_id;
        $this->category->tag_id = $this->tag_id;
        $this->category->is_menu = $this->is_menu;
        $this->category->description = $this->description;
        $this->category->link = $this->convertLink($this->link);
        $this->category->order = $lastOrderCate +1;
        // dd($this->lastOrderCate);
        $this->category->save();
        $this->emit('closeModalCreateEdit');
        if ($this->mode == 'create') {
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => 'Thêm thành công']);
        } else if ($this->mode == 'edit') {
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => 'Sửa thành công']);
        }
        $this->resetInput();
    }
    public function standardData()
    {
        $this->name = trim($this->name);
        $this->link = trim($this->link);
    }
    public function resetInput()
    {
        $this->name = null;
        $this->tag_id = null;
        $this->parent_id = null;
        $this->link = null;
        $this->is_menu = null;
        $this->description = null;
    }
    public function edit($id)
    {
        $this->mode = 'edit';
        $this->categoryId = $id;
        $this->resetValidation();
        $this->category =  Category::find($this->categoryId);
        if ($this->category) {
            $this->name = $this->category->name;
            $this->parent_id = $this->category->parent_id;
            $this->tag_id = $this->category->tag_id;
            $this->link = $this->category->link;
            $this->is_menu = $this->category->is_menu;
            $this->description = $this->category->description;
        }
    }
    public function resetSearch()
    {
        $this->searchName = '';
    }
    public function show($id)
    {
        $this->mode = 'show';
        $this->category =  Category::find($id);
        if ($this->category) {
            $this->name = $this->category->name;
            $this->parent_id = $this->category->parent_id;
            $this->tag_id = $this->category->tag_id;
            $this->link = $this->category->link;
            $this->is_menu = $this->category->is_menu;
            $this->description = $this->category->description;
        }
    }
    public function delete()
    {
        Category::find($this->deleteId)->delete();
        $this->emit('closeModalDelete');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => 'Xóa thành công']);
    }
    public function convertLink($str)
    {
        $unicode = array(

            'a' => 'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',

            'd' => 'đ',

            'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',

            'i' => 'í|ì|ỉ|ĩ|ị',

            'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',

            'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',

            'y' => 'ý|ỳ|ỷ|ỹ|ỵ',

            'A' => 'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',

            'D' => 'Đ',

            'E' => 'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',

            'I' => 'Í|Ì|Ỉ|Ĩ|Ị',

            'O' => 'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',

            'U' => 'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',

            'Y' => 'Ý|Ỳ|Ỷ|Ỹ|Ỵ',

        );

        foreach ($unicode as $nonUnicode => $uni) {

            $str = preg_replace("/($uni)/i", $nonUnicode, $str);
        }
        $str = str_replace(' ', '-', $str);

        return $str;
    }
    public function updateOrder($list)
    {
        $start = -1;
        $end = -1;
        $kt = false;
        // tìm điểm bắt đầu và kết thúc thay đổi khi kéo thả 
        foreach ($this->dataSortable as $key => $value) {
            if (isset($list[$key]['value']) && $list[$key]['value'] != $value['order']) {
                if (!$kt) {
                    $start = $key;
                    $kt = true;
                } else {
                    $end = $key;
                }
            }
        }
        if ($start != -1 && $end != -1) {
            // xử lý khi kéo xuống
            if ($this->dataSortable[$end]['order'] != $list[$start]['value']) {
                foreach ($this->dataSortable as $key => $value) {
                    if ($key >= $start && $key <= $end) {
                        if ($key == $end) {
                            Category::where('id', $this->dataSortable[$start]['id'])->update(['order' => $this->dataSortable[$end]['order']]);
                        } else {
                            Category::where('id', $this->dataSortable[$key + 1]['id'])->update(['order' => $value['order']]);
                        }
                    }
                }
            }
            // xử lý khi kéo lên
            else {
                foreach ($this->dataSortable as $key => $value) {
                    if ($key >= $start && $key <= $end) {
                        if ($key == $start) {
                            Category::where('id', $this->dataSortable[$end]['id'])->update(['order' => $this->dataSortable[$start]['order']]);
                        } else {
                            Category::where('id', $this->dataSortable[$key - 1]['id'])->update(['order' => $this->dataSortable[$key]['order']]);
                        }
                    }
                }
            }
        }
    }
}
