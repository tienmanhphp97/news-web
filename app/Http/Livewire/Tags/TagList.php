<?php

namespace App\Http\Livewire\Tags;

use App\Http\Livewire\Base\BaseLive;
use App\Models\Tag;
use Livewire\Component;

class TagList extends BaseLive
{
    public $mode = 'create';
    public $name;
    public $searchName;
    public $tagId;
    protected $rules = [
        'name' => 'required',
    ];

    protected $messages = [
        'name.required' => 'Tên tag bắt buộc',
    ];
    public function render()
    {
        $query= Tag::query();
        if($this->searchName) {
            $query->where("name", "like", "%".$this->searchName."%");
        }
        $data = $query->orderBy('created_at','desc')->paginate($this->perPage);
        return view('livewire.tags.tag-list', compact('data'));
    }
    public function create()
    {
        $this->resetValidation();
        $this->mode='create';
    }
    public function saveData(){
        $this->validate();
        $this->standardData();
        if($this->mode=='create'){
            $this->tag =  new Tag;
        }else if($this->mode=='edit'){
            $this->tag =  Tag::find($this->tagId);
        }

        $this->tag->name=$this->name;
        $this->tag->save();
        $this->emit('closeModalCreateEdit');
        $this->resetInput();
        if($this->mode=='create') {
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => 'Thêm thành công']);
        }else if($this->mode=='edit'){
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => 'Sửa thành công']);
        }
    }
    public function standardData(){
        $this->name = trim($this->name);
    }
    public function resetInput()
    {
        $this->name = '';
    }
    public function edit($id){
        $this->mode='edit';
        $this->tagId=$id;
        $this->resetValidation();
        $this->tag =  Tag::find($this->tagId);
        $this->name=$this->tag->name;
    }
    public function resetSearch(){
        $this->searchName = '';
    }
    public function show($id){
        $this->mode='show';
        $this->tag =  Tag::find($id);
        if($this->tag){
            $this->name=$this->tag->name;
        }
    }
    public function delete(){
        Tag::find($this->deleteId)->delete();
        $this->emit('closeModalDelete');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => 'Xóa thành công']);
    }
}
