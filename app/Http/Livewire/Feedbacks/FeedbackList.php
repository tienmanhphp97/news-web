<?php

namespace App\Http\Livewire\Feedbacks;

use App\Http\Livewire\Base\BaseLive;
use App\Models\Feedback;
use Livewire\Component;

class FeedbackList extends BaseLive
{

    public $searchName;
    public $feedback;
    public $content;
    public $feedbackId;
    public $editId;
    public $mode;
    public $content_edit;
    public $searchStatus='';
//    public $status;

    public function render()
    {
        $query= Feedback::query();
        if($this->searchName) {
            $query->where("content", "like", "%".$this->searchName."%")
                ->orWhere("email","like", "%".$this->searchName."%")
                ->orWhere("phone","like", "%".$this->searchName."%");
        }
        if($this->searchStatus==1||$this->searchStatus==0){
            $query->where("status", '=',$this->searchStatus);
        }
        $data = $query->whereNull('parent_id')->orderBy('created_at','desc')->paginate($this->perPage);
        return view('livewire.feedbacks.feedback-list', compact('data'));
    }
    public function feedback($id){
        $this->resetValidation();
        $this->feedback =  Feedback::find($id);
        $this->feedbackId=$id;
    }
    public function saveData(){

        $this->standardData();
        $this->feedback =  Feedback::find($this->feedbackId);
        if($this->mode!='edit'){
            $this->validate([
                'content' => 'required',
            ],[
                'content.required' => 'Trường này bắt buộc',
            ]);
            $this->feedback->status=1;
            $this->feedback->save();
            $reply =  new Feedback();
            $reply->content=$this->content;
        }else{
            $this->validate([
                'content_edit' => 'required',
            ],[
                'content_edit.required' => 'Trường này bắt buộc',
            ]);
            $reply =  Feedback::find($this->editId);
            $this->feedback =  Feedback::find($this->feedbackId);
            $reply->content=$this->content_edit;
        }
        $reply->parent_id=$this->feedbackId;
        $reply->save();
        $this->resetInput();
    }
    public function standardData(){
        $this->content = trim($this->content);
    }
    public function resetInput()
    {
        $this->content = '';
        $this->content_edit = '';
        $this->mode='';
        $this->editId='';

    }
    public function edit($id, $content){
        $this->mode='edit';
        $this->content_edit=$content;
        $this->editId=$id;
    }
    public function resetSearch(){
        $this->searchName = '';
        $this->searchStatus = '';
    }
    public function cancel(){
        $this->editId='';
        $this->mode='create';
    }
}
