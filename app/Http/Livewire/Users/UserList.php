<?php

namespace App\Http\Livewire\Users;

use App\Http\Livewire\Base\BaseLive;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Livewire\Component;

class UserList extends BaseLive
{
    public $username;
    public $phone;
    public $fullname;
    public $date_of_birth;
    public $email;
    public $password;
    public $searchName;
    public $userList;
    public $mode = 'create';
    public $userId;
    protected $rules = [
        'username' => 'required',
        'fullname' => 'required',
        'phone' => 'required|max:11|regex:/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/',
        'email' => 'required',
        'password' => 'required',
        'date_of_birth' => 'required',
    ];
    protected $messages = [
        'username.required' => 'Tài khoản bắt buộc',
        'fullname.required' => 'Họ tên bắt buộc',
        'phone.required' => 'Số điện thoại bắt buộc',
        'email.required' => 'Email bắt buộc',
        'password.required' => 'Mật khẩu bắt buộc',
        'date_of_birth.required' => 'Ngày sinh bắt buộc',
        'phone.regex' => 'Số điện thoại không đúng định dạng',
    ];

    public function render()
    {
        $query = User::query();
        if($this->searchName) {
            $query->where("username", "like", "%".$this->searchName."%");
            $query->orWhere("email", "like", "%".$this->searchName."%");
            $query->orWhere("phone", "like", "%".$this->searchName."%");
        }
        $data = $query->orderBy('created_at', 'desc')->paginate($this->perPage);
        return view('livewire.users.user-list', compact('data'));
    }

    public function create()
    {   $this->resetInput();
        $this->resetValidation();
        $this->mode = 'create';
    }

    public function saveData()
    {
        $this->validate();
        if ($this->mode == 'create') {
            $this->userList =  new User;
        } else if ($this->mode == 'edit') {
            $this->userList =  User::find($this->userId);
        }
        $this->userList->username = $this->username;
        $this->userList->phone = $this->phone;
        $this->userList->fullname = $this->fullname;
        $this->userList->date_of_birth = $this->date_of_birth;
        $this->userList->email = $this->email;
        $this->userList->password = Hash::make($this->password);

        $this->userList->save();
        $this->emit('closeModalCreateEdit');
        $this->resetInput();
    }
    public function resetInput()
    {
        $this->username = '';
        $this->phone = '';
        $this->fullname = '';
        $this->date_of_birth = '';
        $this->email = '';
        $this->password = '';
    }
    public function edit($id)
    {
        $this->mode = 'edit';
        $this->userId = $id;
        $this->resetValidation();
        $this->userList =  User::find($this->userId);
        $this->username = $this->userList->username;
        $this->fullname = $this->userList->fullname;
        $this->phone = $this->userList->phone;
        $this->email = $this->userList->email;
        $this->password = '';
        $this->date_of_birth = $this->userList->date_of_birth;
    }
    public function show($id)
    {
        $this->mode = 'show';
        $this->userList =  User::find($id);
        if ($this->userList) {
            $this->username = $this->userList->username;
            $this->fullname = $this->userList->fullname;
            $this->phone = $this->userList->phone;
            $this->email = $this->userList->email;
            $this->password = $this->userList->password;
            $this->date_of_birth = $this->userList->date_of_birth;
        }
    }
    public function resetSearch(){
        $this->searchName = '';
    }
    public function delete(){
        User::find($this->deleteId)->delete();
        $this->emit('closeModalDelete');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => 'Xóa thành công']);
    }
}
