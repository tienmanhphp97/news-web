<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\File;

class UploadController extends Controller
{
    public function uploadImage(Request $request){
        // dd($request);
        $result = $request->file('file')->store('images','public');
        $domain = request()->getSchemeAndHttpHost();
        return $domain .'/storage/'. $result;
    }
}
