<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class News extends Model
{
    use  HasFactory;
    protected $fillable = [
        'content',
        'category_id',
        'description',
        'view',
        'name',
        'meta_title',
        'meta_description',
        'meta_keyword',
        'file_path'
    ];
    protected $table = 'news';
    public function category()
    {
        return $this->belongsTo(Category::class,'category_id');
    }
}

