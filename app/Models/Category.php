<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $fillable = ['name','parent_id','tag_id','is_menu','order'];
    protected $table = 'category';
    public function children(){
        return $this->hasMany(Category::class, 'parent_id');
    }
    public function parent()
    {
        return $this->belongsTo(Category::class,'parent_id');
    }
    public function tag()
    {
        return $this->belongsTo(Tag::class,'tag_id');
    }
}
