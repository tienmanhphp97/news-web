<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    use HasFactory;
    protected $fillable = ['content','parent_id','status','phone','email'];
    protected $table = 'feedback';
    public function children(){
        return $this->hasMany(Feedback::class, 'parent_id');
    }
    public function parent()
    {
        return $this->belongsTo(Feedback::class,'parent_id');
    }
}
