<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasFactory;
    protected $fillable = [
        'url',
        'file_name',
        'model_name',
        'model_id',
        'size_file',
        'type',
        'status',
    ];
    protected $table = 'files';
}
