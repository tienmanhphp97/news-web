$(function() {
    'use strict'
    // window.addEventListener('setSelect2', event => {
    //     let id = $('#buyDateEdit').attr('id');
    //     $(".select2-box1").select2({
    //         width: 'resolve',
    //     });
    // });

    window.livewire.on('closeModalCreateEdit', () => {
        $('#modelCreateEdit').modal('hide');
        document.getElementsByClassName('modal-backdrop')[0].remove();
    });
    window.livewire.on('closeModalDelete', () => {
        $('#deleteModal').modal('hide');
        document.getElementsByClassName('modal-backdrop')[0].remove();

    });
    // $(".select2-box").select2({
    //     placeholder: "Chọn loại",
    //     allowClear: true,
    // });
    // window.livewire.on('setSelect2Input', () => {
    //     $(".select2-box").select2({
    //         placeholder: "Chọn loại",
    //         allowClear: true,
    //         }
    //     );
    // });

    $('[data-toggle="tooltip"]').tooltip();
});
