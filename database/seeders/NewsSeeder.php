<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class NewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('news')->insert([
            [
                'name'=>'Vũ Hoàng Nhật Tân',
                'description'=>'Luật sư',
                'file_path'=>'/assets/images/MrTan.jpg',
                'category_id'=>5
            ],
            [
                'name'=>'Đỗ Đình Sơn',
                'description'=>'Luật sư',
                'file_path'=>'/assets/images/MrSon.jpg',
                'category_id'=>5
            ],
            [
                'name'=>'Phạm Hồng Nhung',
                'description'=>'Luật sư',
                'file_path'=>'/assets/images/MrsNhung.jpg',
                'category_id'=>5
            ],

        ]);

    }
}
