<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category')->insert([
            [
               'id'=>1,
               'name'=>'About us',
               'parent_id'=>null,
               'is_menu'=>1,
               'order'=>1
            ],
            [
               'id'=>2,
               'name'=>'Giới thiệu công ty',
               'parent_id'=>1,
               'is_menu'=>1,
               'order'=>2
            ],
            [
               'id'=>3,
               'name'=>'Profile',
               'parent_id'=>1,
               'is_menu'=>1,
               'order'=>3
            ],
            [
               'id'=>4,
               'name'=>'Đội ngũ',
               'parent_id'=>null,
               'is_menu'=>1,
               'order'=>4
            ],
            [
               'id'=>5,
               'name'=>'Profile luật sư / nhân viên',
               'parent_id'=>4,
               'is_menu'=>1,
               'order'=>5
            ],
            [
               'id'=>6,
               'name'=>'Tin tức',
               'parent_id'=>null,
               'is_menu'=>1,
               'order'=>6
            ],
            [
               'id'=>7,
               'name'=>'Tin tức cập nhật văn bản pháp lý',
               'parent_id'=>6,
               'is_menu'=>1,
               'order'=>7
            ],
            [
               'id'=>8,
               'name'=>'Tin tức hoạt động của công ty',
               'parent_id'=>6,
               'is_menu'=>1,
               'order'=>8
            ],
            [
               'id'=>9,
               'name'=>'Thời sự / tin tức khác',
               'parent_id'=>6,
               'is_menu'=>1,
               'order'=>9
            ],
            [
               'id'=>10,
               'name'=>'Thủ tục - Giấy phép',
               'parent_id'=>null,
               'is_menu'=>1,
               'order'=>10
            ],
            [
               'id'=>11,
               'name'=>'DOANH NGHIỆP',
               'parent_id'=>10,
               'is_menu'=>1,
               'order'=>11
            ],
            [
               'id'=>12,
               'name'=>'SỞ HỮU TRÍ TUỆ',
               'parent_id'=>10,
               'is_menu'=>1,
               'order'=>12
            ],
            [
               'id'=>13,
               'name'=>'ĐẦU TƯ',
               'parent_id'=>10,
               'is_menu'=>1,
               'order'=>13
            ],
            [
               'id'=>14,
               'name'=>'GIẤY PHÉP',
               'parent_id'=>10,
               'is_menu'=>1,
               'order'=>14
            ],
            [
               'id'=>15,
               'name'=>'KẾ TOÁN THUẾ',
               'parent_id'=>10,
               'is_menu'=>1,
               'order'=>15
            ],
            [
               'id'=>16,
               'name'=>'Dịch vụ chuyên sâu',
               'parent_id'=>null,
               'is_menu'=>1,
               'order'=>16
            ],
            [
               'id'=>17,
               'name'=>'Rà soát pháp lý',
               'parent_id'=>16,
               'is_menu'=>1,
               'order'=>17
            ],
            [
               'id'=>18,
               'name'=>'Giải quyết tranh chấp',
               'parent_id'=>16,
               'is_menu'=>1,
               'order'=>18
            ],
            [
               'id'=>19,
               'name'=>'M & A',
               'parent_id'=>16,
               'is_menu'=>1,
               'order'=>19
            ],
            [
               'id'=>20,
               'name'=>'Thị trường vốn',
               'parent_id'=>16,
               'is_menu'=>1,
               'order'=>20
            ],
            [
               'id'=>21,
               'name'=>'Bất động sản',
               'parent_id'=>16,
               'is_menu'=>1,
               'order'=>21
            ],
            [
               'id'=>22,
               'name'=>'Start-up Công nghệ',
               'parent_id'=>16,
               'is_menu'=>1,
               'order'=>22
            ],
            [
               'id'=>23,
               'name'=>'Tuyển dụng',
               'parent_id'=>null,
               'is_menu'=>null,
               'order'=>23
            ],
            [
               'id'=>24,
               'name'=>'Liên hệ',
               'parent_id'=>null,
               'is_menu'=>null,
               'order'=>24
            ],
            [
               'id'=>25,
               'name'=>'Thành lập Công ty / Doanh nghiệp',
               'parent_id'=>11,
               'is_menu'=>1,
               'order'=>25
            ],
            [
               'id'=>26,
               'name'=>'Đăng ký kinh doanh',
               'parent_id'=>11,
               'is_menu'=>1,
               'order'=>26
            ],
            [
               'id'=>27,
               'name'=>'Thay đổi đăng ký kinh doanh',
               'parent_id'=>11,
               'is_menu'=>1,
               'order'=>27
            ],
            [
               'id'=>28,
               'name'=>'Tư vấn pháp luật thường xuyên',
               'parent_id'=>11,
               'is_menu'=>1,
               'order'=>28
            ],
            [
               'id'=>29,
               'name'=>'Thành lập chi nhánh công ty',
               'parent_id'=>11,
               'is_menu'=>1,
               'order'=>29
            ],
            [
               'id'=>30,
               'name'=>'Thành lập văn phòng đại diện',
               'parent_id'=>11,
               'is_menu'=>1,
               'order'=>30
            ],
            [
               'id'=>31,
               'name'=>'Thủ tục chuyển đổi loại hình doanh nghiệp',
               'parent_id'=>11,
               'is_menu'=>1,
               'order'=>31
            ],
            [
               'id'=>32,
               'name'=>'Tư vấn mua bán doanh nghiệp',
               'parent_id'=>11,
               'is_menu'=>1,
               'order'=>32
            ],
            [
               'id'=>33,
               'name'=>'Giải thể doanh nghiệp',
               'parent_id'=>11,
               'is_menu'=>1,
               'order'=>33
            ],
            [
               'id'=>34,
               'name'=>'Dịch vụ luật sư riêng cho doanh nghiệp',
               'parent_id'=>11,
               'is_menu'=>1,
               'order'=>34
            ],
            [
               'id'=>35,
               'name'=>'Tư vấn hợp đồng',
               'parent_id'=>11,
               'is_menu'=>1,
               'order'=>35
            ],
            [
               'id'=>36,
               'name'=>'Tư vấn thuế',
               'parent_id'=>11,
               'is_menu'=>1,
               'order'=>36
            ],
            [
               'id'=>37,
               'name'=>'Giải quyết tranh chấp hợp đồng',
               'parent_id'=>11,
               'is_menu'=>1,
               'order'=>37
            ],
            [
               'id'=>38,
               'name'=>'Tư vấn pháp luật hợp đồng',
               'parent_id'=>11,
               'is_menu'=>1,
               'order'=>38
            ],
            [
               'id'=>39,
               'name'=>'Thành lập công ty 100% vốn nước ngoài',
               'parent_id'=>13,
               'is_menu'=>1,
               'order'=>39
            ],
            [
               'id'=>40,
               'name'=>'Thành lập doanh nghiệp có vốn đầu tư nước ngoài',
               'parent_id'=>13,
               'is_menu'=>1,
               'order'=>40
            ],
            [
               'id'=>41,
               'name'=>'Điều chỉnh giấy chứng nhận đầu tư',
               'parent_id'=>13,
               'is_menu'=>1,
               'order'=>41
            ],
            [
               'id'=>42,
               'name'=>'Thành lập văn phòng đại diện công ty nước ngoài tại Việt Nam',
               'parent_id'=>13,
               'is_menu'=>1,
               42
            ],
            [
               'id'=>43,
               'name'=>'Nhà đầu tư nước ngoài góp vốn vào công ty Việt Nam',
               'parent_id'=>13,
               'is_menu'=>1,
               'order'=>43
            ],
            [
               'id'=>44,
               'name'=>'Thủ tục đầu tư ra nước ngoài',
               'parent_id'=>13,
               'is_menu'=>1,
               'order'=>44
            ],
            [
               'id'=>45,
               'name'=>'Cấp giấy phép cho nhà thầu nước ngoài',
               'parent_id'=>13,
               'is_menu'=>1,
               'order'=>45
            ],
            [
               'id'=>46,
               'name'=>'Thành lập chi nhánh của thương nhân nước ngoài',
               'parent_id'=>13,
               'is_menu'=>1,
               'order'=>46
            ],
            [
               'id'=>47,
               'name'=>'Tư vấn chuyển nhượng dự án',
               'parent_id'=>13,
               'is_menu'=>1,
               'order'=>47
            ],
            [
               'id'=>48,
               'name'=>'Tư vấn pháp lý quá trình hoạt động dự án',
               'parent_id'=>13,
               'is_menu'=>1,
               'order'=>48
            ],
            [
               'id'=>49,
               'name'=>'Giấy phép lao động',
               'parent_id'=>14,
               'is_menu'=>1,
               'order'=>49
            ],
            [
               'id'=>50,
               'name'=>'Giấy phép kinh doanh lữ hành quốc tế',
               'parent_id'=>14,
               'is_menu'=>1,
               'order'=>50
            ],
            [
               'id'=>51,
               'name'=>'Giấy phép kinh doanh lữ hành nội địa',
               'parent_id'=>14,
               'is_menu'=>1,
               'order'=>51
            ],
            [
               'id'=>52,
               'name'=>'Giấy phép mạng xã hội',
               'parent_id'=>14,
               'is_menu'=>1,
               'order'=>52
            ],
            [
               'id'=>53,
               'name'=>'Giấy chứng nhận vệ sinh an toàn thực phẩm',
               'parent_id'=>14,
               'is_menu'=>1,
               'order'=>53
            ],
            [
               'id'=>54,
               'name'=>'Công bố phù hợp quy định an toàn thực phẩm',
               'parent_id'=>14,
               'is_menu'=>1,
               'order'=>54
            ],
            [
               'id'=>55,
               'name'=>'Visa cho người nước ngoài',
               'parent_id'=>14,
               'is_menu'=>1,
               'order'=>55
            ],
            [
               'id'=>56,
               'name'=>'Thẻ tạm trú cho người nước ngoài',
               'parent_id'=>14,
               'is_menu'=>1,
               'order'=>56
            ],
            [
               'id'=>57,
               'name'=>'Công bố lưu hành mỹ phẩm',
               'parent_id'=>14,
               'is_menu'=>1,
               'order'=>57
            ],
            [
               'id'=>58,
               'name'=>'Thành lập trung tâm ngoại ngữ',
               'parent_id'=>14,
               'is_menu'=>1,
               'order'=>58
            ],
            [
               'id'=>59,
               'name'=>'Tư vấn kế toán thuế',
               'parent_id'=>15,
               'is_menu'=>1,
               'order'=>59
            ],
            [
               'id'=>60,
               'name'=>'Dịch vụ báo cáo thuế',
               'parent_id'=>15,
               'is_menu'=>1,
               'order'=>60
            ],
            [
               'id'=>61,
               'name'=>'Dịch vụ bảo hiểm xã hội',
               'parent_id'=>15,
               'is_menu'=>1,
               'order'=>61
            ],
            [
               'id'=>62,
               'name'=>'Dịch vụ quyết toán thuế thu nhập cá nhân',
               'parent_id'=>15,
               'is_menu'=>1,
               'order'=>62
            ],
            [
               'id'=>63,
               'name'=>'Tư vấn pháp luật thuế cho công ty có vốn nước ngoài',
               'parent_id'=>15,
               'is_menu'=>1,
               'order'=>63
            ],
            [
               'id'=>64,
               'name'=>'Tư vấn thuế thu thập doanh nghiệp',
               'parent_id'=>15,
               'is_menu'=>1,
               'order'=>64
            ],
            [
               'id'=>65,
               'name'=>'Tư vấn thuế cho văn phòng đại diện nước ngoài',
               'parent_id'=>15,
               'is_menu'=>1,
               'order'=>65
            ],
            [
               'id'=>66,
               'name'=>'Tư vấn pháp luật thuế giá trị gia tăng',
               'parent_id'=>15,
               'is_menu'=>1,
               'order'=>66
            ],
            [
               'id'=>67,
               'name'=>'Tư vấn đăng ký nhãn hiệu',
               'parent_id'=>12,
               'is_menu'=>1,
               'order'=>67
            ],
            [
               'id'=>68,
               'name'=>'Đăng ký chỉ dẫn địa lý',
               'parent_id'=>12,
               'is_menu'=>1,
               'order'=>68
            ],
            [
               'id'=>69,
               'name'=>'Trình tự thủ tục đăng ký nhãn hiệu',
               'parent_id'=>12,
               'is_menu'=>1,
               'order'=>69
            ],
            [
               'id'=>70,
               'name'=>'Đăng ký kiểu dáng công nghiệp',
               'parent_id'=>12,
               'is_menu'=>1,
               'order'=>70
            ],
            [
               'id'=>71,
               'name'=>'Đăng ký sáng chế',
               'parent_id'=>12,
               'is_menu'=>1,
               'order'=>71
            ],
            [
               'id'=>72,
               'name'=>'Đăng ký bảo hộ quyền tác giả',
               'parent_id'=>12,
               'is_menu'=>1,
               'order'=>72
            ],
            [
               'id'=>73,
               'name'=>'Xử lý vi phạm nhãn hiệu',
               'parent_id'=>12,
               'is_menu'=>1,
               'order'=>73
            ],
            [
               'id'=>74,
               'name'=>'Thủ tục cấp phép sử dụng tác phẩm Âm nhạc',
               'parent_id'=>12,
               'is_menu'=>1,
               'order'=>74
            ],
            [
               'id'=>75,
               'name'=>'Chấm dứt hiệu lực Giấy chứng nhận đăng ký nhãn hiệu',
               'parent_id'=>12,
               'is_menu'=>1,
               'order'=>75
            ],
            [
               'id'=>76,
               'name'=>'Khiếu nại sở hữu trí tuệ',
               'parent_id'=>12,
               'is_menu'=>1,
               'order'=>76
            ],
            [
               'id'=>77,
               'name'=>'Xin cấp phép sử dụng tác phẩm âm nhạc',
               'parent_id'=>12,
               'is_menu'=>1,
               'order'=>77
            ],
            [
               'id'=>78,
               'name'=>'Nhượng quyền thương mại',
               'parent_id'=>12,
               'is_menu'=>1,
               'order'=>78
            ],

        ]);

    }
}
